class Heap {
  constructor(isMax) {
    this.array = [];
    this._size = 0;
    this.isMax = isMax;
  }

  get size() {
    return this._size;
  }

  pop(){
    if(!this.array) {
      return;
    }

    this.__swap(0, this.array.length - 1);
    const result = this.array.pop();
    this.__heapifyDown(0);
    this._size--;
    return result;
  }

  push(num){
    let n = this.array.push(num) - 1;
    this.__heapifyUp(n);
    this._size++;
  }

  __heapifyUp(start){
    if(start === 0) {
      return;
    }

    let parent = this.__getParent(start);
    if (((this.array[parent] > this.array[start]) && !this.isMax)
      ||((this.array[parent] < this.array[start]) && this.isMax)) {
      this.__swap(parent, start);
      return this.__heapifyUp(parent);
    }
    return;
  }

  __heapifyDown(start){
    if(start >= this.array.length) {
      return;
    }

    let lChild = this.__getLeftChild(start);
    let rChild = this.__getRighChild(start);

    let bChild = lChild;

    if (bChild >= this.array.length ) {
      return;
    } else if (rChild < this.array.length){
      if(!this.isMax) {
        bChild = this.array[lChild] < this.array[rChild] ? lChild : rChild;
      } else {
        bChild = this.array[lChild] > this.array[rChild] ? lChild : rChild;
      }
    }

    if (((this.array[bChild] < this.array[start]) && !this.isMax)
      ||((this.array[bChild] > this.array[start]) && this.isMax)) {
      this.__swap(bChild, start);
      return this.__heapifyDown(bChild);
    }

    return;
  }

  __getLeftChild(n) {
    return Math.floor((n * 2) + 1);
  }

  __getRighChild(n) {
    return Math.floor((n * 2) + 2);
  }

  __getParent(n) {
    return Math.floor((n-1)/2);
  }

  __swap(i, j) {
    let tmp = this.array[i];
    this.array[i] = this.array[j];
    this.array[j] = tmp;
  }

}


//tests
const arr = [4, 6, 3, 87, 23, 46, 73, 59, 82];
const myheap = new Heap(true)
while (arr.length > 0) {
  myheap.push(arr.pop());
}

while (myheap.size) {
  console.log(myheap.pop());
}

module.exports = Heap;
