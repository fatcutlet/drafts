function greatherIsBigger(str) {
  const strArr = [...str];
  let arrN = [];

  for (let i = 0; i < str.length; i++) {
    let el = strArr.pop();
    //insert to sorted arr
    pushSorted(arrN, el);

    //form new str
    let end = strArr.length - 1;
    for(let j = 0; j < arrN.length; j++) {
      if(arrN[j] <= strArr[end]) {
        continue;
      }

      let result = strArr.slice(0, end);
      result.push(arrN.splice(j, 1)[0]);

      pushSorted(arrN, strArr[end]);

      for(let k = 0; k < arrN.length; k++) {
        result.push(arrN[k]);
      }

      result = result.join("");
      if(result > str) return result;
    }
  }

  return "no answer"
}

function pushSorted(arrN, el) {
  for(let j = 0; j < arrN.length; j++) {
    if(el < arrN[j]) {
      //swap
      let tmp = arrN[j];
      arrN[j] = el;
      el = tmp;
    }
  }
  arrN.push(el);
}

console.log(greatherIsBigger("dkhc"));
