function organizingContainers(container) {
  let possible = true;

  //count sum of rows:
  let colSums = container.map( (val, idx) => {
    return container.reduce( (acc, cval) => {
      return acc + cval[idx];
    }, 0);
  });

  let rowSums = container.map( val => {
    return val.reduce( (acc, cval) => {
      return cval + acc;
    }, 0);
  });
  
  rowSums.sort();
  colSums.sort();

  rowSums.forEach( (val, idx) => {
    if (val != colSums[idx]) possible = false;
  })

  return possible ? "Possible" : "Impossible";
}

let container = [ [ 1, 1 ], [ 1, 1 ] ];
console.log(organizingContainers(container));
container = [ [ 0, 2 ], [ 1, 1 ] ];
console.log(organizingContainers(container));
container = [[1, 3, 1], [2, 1, 2], [3, 3, 3]];
console.log(organizingContainers(container));
container = [[0, 2, 1], [1, 1, 1], [2, 0, 0]];
console.log(organizingContainers(container));
