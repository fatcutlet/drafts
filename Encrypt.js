function encryption(s) {
  const spaces = /\s/g;

  s = s.replace(spaces, "");

  const sqrt = Math.sqrt(s.length);
  let r = Math.floor(sqrt);
  let c = Math.ceil(sqrt);
  if((r * c) < s.length) r++;

  const result = [];
  for (let i = 0; i < r; i++) {
    let start = (i * c);
    let end = start + c < s.length ? start + c : s.length;
    result.push(s.substring(start, end))
  }

  const final = [];
  for (let i = 0; i < c; i++) {
    for (let j = 0; j < r; j++) {
      if(result[j][i]) {
        final.push(result[j][i]);
      }
    }
    final.push(" ");
  }

  const finalStr = final.join("");
  console.log(finalStr);
}

encryption("feed the dog");
encryption("chillout");
