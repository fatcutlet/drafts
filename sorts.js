function generate_array(len) {
  let result = new Array(len).fill(0);
  for(let i in result) {
    result[i] = Math.floor((Math.random() * 1000));
  }
  return result;
}

function swap(arr, i, j) {
  tmp = arr[i];
  arr[i] = arr[j];
  arr[j] = tmp;
}

/*Buble sort*/
function bubble_sort(arr, count) {
  if(!count) {
    count = 0;
  }

  let wasSwaps = false;
  for(let i = 1; i < arr.length; i++) {
    if (arr[i - 1] > arr[i]) {
      let tmp = arr[i - 1];
      arr[i - 1] = arr[i];
      arr[i] = tmp;
      wasSwaps = true;
      count++;
    }
  }
  if (!wasSwaps) return count;
  return bubble_sort(arr, count);
}

function test_buble(num) {
  let arr = generate_array(num);
  let swaps = bubble_sort(arr);
  console.log(arr);
  console.log(`Cost: ${swaps}`);
}
/*Buble sort end*/

/*Merge sort*/
function mergeSort(arr, count) {
  if(!count) {
    count = 0;
  }

  if (arr.length == 1) {
    return [arr, count];
  }

  let mid = arr.length / 2;
  let left = mergeSort(arr.slice(0, mid), count);
  let right = mergeSort(arr.slice(mid), count);

  count += left[1];
  count += right[1];

  left = left[0];
  right = right[0];

  let merged = [];

  while (left.length > 0 && right.length > 0) {
    let el;
    if (left[0] < right[0]) {
      el = left.shift();
    } else {
      el = right.shift();
    }
    merged.push(el);
    count++;
  }

  while (left.length > 0) {
    merged.push(left.shift());
    count++;
  }
  while (right.length > 0) {
    merged.push(right.shift());
    count++;
  }

  return [merged, count]
}

function testMerge(num) {
  let arr = generate_array(num);
  let results = mergeSort(arr);
  console.log(results[0]);
  console.log(`Cost: ${results[1]}`);
}
/*Merge sort end*/

/*Quick sort*/
function quickSort(arr, count, start, end) {
  if(count == undefined) {
    count = 0;
  }

  if(start == undefined) {
    start = 0;
  }

  if(end == undefined) {
    end = arr.length - 1;
  }

  //base case
  if((end - start + 1) <= 1) {
    return count;
  }

  //random mid
  let mid = Math.floor(Math.random() * (end - start)) + start;
  //mid to end
  swap(arr, mid, end);
  count++;

  //left pointer
  let left = start;
  //right pointer
  let right = end - 1;

  //partition
  while (left != right) {
    if (arr[left] <= arr[end]) {
      left++;
    } else if (arr[right] > arr[end]) {
      right--;
    } else {
      swap(arr, left, right);
    }
    count++;
  }

  if(arr[left] > arr[end]){
    swap(arr, left, end);
  } else {
    left = end;
  }
  count++;

  count += quickSort(arr, 0, start, left - 1);
  count += quickSort(arr, 0, left + 1, end);
  return count;

}

function testQuick(num) {
  let arr = generate_array(num);
  let cost = quickSort(arr);
  console.log(arr);
  console.log(`Cost: ${cost}`);
}
/*Quick sort end*/

/*Radix sort*/
function getSigNum(fullnum, i) {
  //min i = 1
  let a = Math.pow(10, i);
  let b = Math.pow(10, i - 1);
  return Math.trunc((fullnum % a) / b)
}

/*
p - position
sort array of numbers according position
*/
function countSort(arr, p) {

  const cArr = new Array(10).fill(0);
  for (let i in arr) {
    let sn = getSigNum(arr[i], p)
    cArr[sn]++;
  }

  for (let i = 1; i < cArr.length; i++) {
    cArr[i] += cArr[i-1];
  }

  //reverse order to make it stable
  const out = new Array(arr.length);
  for(let i = arr.length - 1; i >= 0; i--) {
    let sn = getSigNum(arr[i], p);
    let ni = cArr[sn] - 1;
    out[ni] = arr[i];
    cArr[sn]--;
  }

  return out;
}

function radixSort(arr) {
  let max = Math.max(...arr);
  for (let p = 1; Math.pow(10, p-1) < max; p++) {
    arr = countSort(arr, p);
  }
  return arr;
}

function testRadix(num) {
  let arr = generate_array(num);
  let results = radixSort(arr);
  console.log(results);
}

/*Radix sort*/

/*main*/
/*
console.log("Do bubble sort 1k");
test_buble(1000);
console.log("Do merge sort 10k");
testMerge(10000);
console.log("Do quick sort 10k");
testQuick(10000);
*/
testRadix(100);
