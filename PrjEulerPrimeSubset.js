function processData(input) {
    //Enter your code here
    /*
    <15
    2, 3, 5, 7, 11, 13, 17
    {2} {3} {5} {7} {2, 3} {2, 5} {2, 3, 5, 7} {11} {2, 11} {13} {2, 3, 5, 7, 11, 13}  {17} {17, 2} {19} {19, 2}
    9

    4 - 3 (2 + 1)
    5 - 3 (2 + 1)
    6 - 5 (3 + 2)
    7 -  5 (3 + 2)
    8 - 7 (4 + 3)
    9 - 7 (3 + 2)
    10 - 7 (4 + 3)
    11 - 7 (4 + 3)
    12 - 9 (5 + 4)
    13 - 9 (5 + 4)
    14 - 11 (6 + 5)
    15 - 11 (6 + 5)
    16 - 11 (6 + 5)
    17 - 11 (6 + 5)
    18 - 13 (7 + 6)
    20 - 15 (8 + 7)
    */
    const primes = new Array(7000).fill(true);
    const results = new Array(7000);
    primes[0] = false;
    primes[1] = false;
    for(let i = 2; i < primes.length; i++) {
      if(primes[i] === true) {
        for(let p = i * 2; p < primes.length; p += i) {
          primes[p] = false;
        }
      }
    }
    for(let i = 0; i < 100; i++) {
      if (primes[i] === true) {
        console.log(i);
      }
    }

}

console.log(processData([10, 4]));
